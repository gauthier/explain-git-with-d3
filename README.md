﻿explain-git-with-d3
=====================
Outil graphique et intéractif pour visualiser les opérations simples de branchement git.

Ce projet est conçu pour aider les gens à comprendre visuellement certains concepts fondamentaux de git.

C'est ma première tentative d'utilisation de SVG et de D3. J'espère que cela vous sera utile.

La page peut être consultée sur le site: https://gauthier.frama.io/explain-git-with-d3/

La version originale en anglais est disponible sur : http://onlywei.github.io/explain-git-with-d3/
